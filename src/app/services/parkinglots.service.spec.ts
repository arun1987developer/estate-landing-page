import { TestBed } from '@angular/core/testing';

import { ParkinglotsService } from './parkinglots.service';

describe('ParkinglotsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParkinglotsService = TestBed.get(ParkinglotsService);
    expect(service).toBeTruthy();
  });
});
