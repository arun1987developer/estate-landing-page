import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParkinglotsService {
  public listing_data =[
    {"name":"Valley ride apartment","address":"200, Village West drive, New ALbany, Indiana, 47150", "rating":[1,2,3,4], "hosts":25, "image": "assets/images/new-comfortable-two-storied-cottage-with-steep-shingle-roof-satellite-dish-stucco-wall_127089-2444.jpg", "price":450 },
    {"name":"Sunrise apartment","address":"200, Village West drive, New ALbany, Indiana, 47150", "rating":[1,2,3], "hosts":25, "image": "assets/images/stay-home-concept-wooden-table-side-view-hand-holding-wooden-cube.jpg", "price":850 },
    {"name":"Valley ride apartment","address":"200, Village West drive, New ALbany, Indiana, 47150", "rating":[1,2,3,4,5], "hosts":25, "image": "assets/images/modern-house-with-sky_35076-483.jpg", "price":350 },
    {"name":"Valley ride apartment","address":"200, Village West drive, New ALbany, Indiana, 47150", "rating":[1,2,3], "hosts":25, "image": "assets/images/modern-house-.jpg", "price":850 }
  ]
  constructor() { }

  recommendListings(){

      return this.listing_data;

  }
}
