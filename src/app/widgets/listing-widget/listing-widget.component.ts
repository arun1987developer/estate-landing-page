import { Component, OnInit } from '@angular/core';
import { ParkinglotsService } from '../../services/parkinglots.service'

@Component({
  selector: 'app-listing-widget',
  templateUrl: './listing-widget.component.html',
  styleUrls: ['./listing-widget.component.scss']
})
export class ListingWidgetComponent implements OnInit {
  public listingdata:any = []
  public loader = false;
  constructor(private dataservice: ParkinglotsService) {}

  ngOnInit() {
    this.getdata();
  }

  getdata(){
    console.log("timer")
    setTimeout(()=>{
      debugger
      this.listingdata = this.dataservice.recommendListings()
      this.loader = true;
      console.log(this.listingdata)
    },5000)


  }

}
