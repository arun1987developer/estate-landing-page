import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-widget',
  templateUrl: './search-widget.component.html',
  styleUrls: ['./search-widget.component.scss']
})
export class SearchWidgetComponent implements OnInit {
  maxView = 'year';
  minView = 'minute';
  minuteStep = 5;
  selectedDate: Date;
  showCalendar = true;
  startView = 'day';
  views = ['minute', 'hour', 'day', 'month', 'year'];


  constructor() { }

  ngOnInit() {
  }


}
