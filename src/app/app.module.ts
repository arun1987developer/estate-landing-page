import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { UikitModule } from './uikit/uikit.module'
import { AppRoutingModule } from './app-routing.module';
import { ParkinglotsService } from './services/parkinglots.service'
import { AppComponent } from './app.component';
import { uiComponents } from './components';
import { HomeComponent } from './views/pages/home/home.component';
import { AppBannerComponent } from './components/app-banner/app-banner.component';
import { SearchWidgetComponent } from './widgets/search-widget/search-widget.component';
import { ListingWidgetComponent } from './widgets/listing-widget/listing-widget.component';

@NgModule({
  declarations: [

    AppComponent,
    uiComponents,
    HomeComponent,
    AppBannerComponent,
    SearchWidgetComponent,
    ListingWidgetComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    UikitModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  providers: [ParkinglotsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
