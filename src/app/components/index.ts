import { AppHeaderComponent } from './app-header/app-header.component'
import { AppFooterComponent } from './app-footer/app-footer.component'
import { AppBannerComponent } from './app-banner/app-banner.component'

export const uiComponents = [AppHeaderComponent, AppFooterComponent]

export * from './app-header/app-header.component'
export * from './app-footer/app-footer.component'
export * from './app-banner/app-banner.component'
